/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author andre
 */
public class ElementByEletronicConf extends Element implements Comparable<ElementByEletronicConf> {
    
    private Integer Ocurrences;
    
    public ElementByEletronicConf(String eletronic_conf, int ocurr) {
        super();
        super.setElectron_configuration(eletronic_conf);
        this.Ocurrences=ocurr;
    }
    
    public ElementByEletronicConf(Element e) {
        super(e);
    }

    public Integer getOcurrences() {
        return this.Ocurrences;
    }

    public void setOcurrences(Integer Ocurrences) {
        this.Ocurrences = Ocurrences;
    }

    @Override
    public int compareTo(ElementByEletronicConf o) {
        if (this.getOcurrences() > o.getOcurrences()) {
            return -1;
        }else if(this.getOcurrences()<o.getOcurrences()){
            return 1;
        }else{
            return this.getElectron_configuration().compareTo(o.getElectron_configuration());           
        }
    }
    
    @Override
    public String toString() {
        return super.getElectron_configuration();
    }
}
