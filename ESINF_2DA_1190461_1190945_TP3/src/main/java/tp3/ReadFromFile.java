/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * class to read the file
 *
 * @author 1190461 & 1190945
 */
public class ReadFromFile {

     /**
     * Method that reads the elements
     *
     * @param file_name
     * @return a linked list with the information
     * @throws FileNotFoundException
     */
    public static LinkedList<Element> readElements(String file_name) throws FileNotFoundException {
        LinkedList<Element> list = new LinkedList<>();
        File file = new File(file_name);
        Scanner ler = new Scanner(file);
        String cabecalho = ler.nextLine();
        while (ler.hasNext()) {
            String line = ler.nextLine();
            Element e = lerLinha(line);
            list.add(e);
        }
        ler.close();
        return list;
    }

    /**
     * Method that reads a line of the file
     *
     * @param line
     * @return an element
     */
    public static Element lerLinha(String line) {
        String[] st = line.split(",");
        Integer atomic_number = verifyInt(st[0]);
        String element = st[1];
        String symbol = st[2];
        Double atomic_weight = verifyDouble(st[3]);
        Double atomic_mass = verifyDouble(st[4]);
        Integer period = verifyInt(st[5]);
        Integer group = verifyInt(st[6]);
        String phase = st[7];
        String most_stable_crystal = st[8];
        String type = st[9];
        Double ionic_radius = verifyDouble(st[10]);
        Double atomic_radius = verifyDouble(st[11]);
        Double electronegativity = verifyDouble(st[12]);
        Double first_ionization_potential = verifyDouble(st[13]);
        Double density = verifyDouble(st[14]);
        Double melting_point = verifyDouble(st[15]);
        Double boiling_point = verifyDouble(st[16]);
        Integer isotepes = verifyInt(st[17]);
        String discoverer = st[18];
        Integer year_of_discovery = verifyInt(st[19]);
        Double specific_heat_capacity = verifyDouble(st[20]);
        String electron_configuration = st[21];
        Integer display_row = Integer.parseInt(st[22]);
        Integer display_column = Integer.parseInt(st[23]);
        Element e = new Element(atomic_number, element, symbol, atomic_weight, atomic_mass, period, group, phase, most_stable_crystal,
                type, ionic_radius, atomic_radius, electronegativity, first_ionization_potential, density, melting_point, boiling_point,
                isotepes, discoverer, year_of_discovery, specific_heat_capacity, electron_configuration, display_row, display_column);
        return e;
    }

    public static Double verifyDouble(String test) {
        double value;
        if (test.equalsIgnoreCase("")) {
            value = 0;
        } else{
            value = Double.parseDouble(test);
        }
        return value;
    }

    public static Integer verifyInt(String test) {
        int value;
        if (test.equalsIgnoreCase("")) {
            value = 0;
        } else{
            value =  Integer.parseInt(test);
        }
        return value;
    }

}
