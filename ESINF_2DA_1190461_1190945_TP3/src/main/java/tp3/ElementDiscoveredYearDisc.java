/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author andre
 */
public class ElementDiscoveredYearDisc extends Element implements Comparable<ElementDiscoveredYearDisc>{
    public ElementDiscoveredYearDisc(String discovered, int yearOfDiscovered){
        super();
        super.setDiscoverer(discovered);
        super.setYear_of_discovery(yearOfDiscovered);
    }
    
    public ElementDiscoveredYearDisc(Element e) {
        super(e);
    }

    @Override
    public int compareTo(ElementDiscoveredYearDisc element) {
        if (element.getDiscoverer().compareTo(this.getDiscoverer())!=0) {
            return this.getDiscoverer().compareTo(element.getDiscoverer());
        }else if(element.getYear_of_discovery().compareTo(this.getYear_of_discovery())!=0){
            return element.getYear_of_discovery().compareTo(this.getYear_of_discovery());
        }else{
            return this.getAtomic_mass().compareTo(element.getAtomic_mass());
        }
    }

    @Override
    public String toString() {
        return super.getSymbol() + " - " + super.getElement() + " - " + super.getAtomic_number() + " - " + super.getAtomic_mass();
    }
}
