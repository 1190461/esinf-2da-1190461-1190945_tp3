/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import Tree.AVL;
import Tree.BST;
import Tree.TREE;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;

/**
 * class for the exercices
 *
 * @author 1190461 & 1190945
 */
public class PeriodicTableExs {

    /**
     * Variable representative of the BST for Atomic Number
     */
    public BST<ElementAtomicNumber> treeAtomicNumber;

    /**
     * Variable representative of the BST for Atomic Mass
     */
    public BST<ElementAtomicMass> treeAtomicMass;

    /**
     * Variable representative of the BST for Element Name
     */
    public BST<ElementByName> treeByName;

    /**
     * Variable representative of the BST for Element Symbol
     */
    public BST<ElementBySymbol> treeBySymbol;

    /**
     * Variable representative of the BST for Element by Discovered and year of
     * discovered
     */
    public BST<ElementDiscoveredYearDisc> treeByDiscoveredYearDisc;

    /**
     * Constructor of PeriodicTableExs
     *
     * @param treeAtomicNumber BST by atomic number
     * @param treeAtomicMass BST by atomic mass
     * @param treeByName BST by element name
     * @param treeBySymbol BST by element symbol
     * @param treeByDiscoveredYearDisc BST by element discovered and year of
     * discover
     */
    public PeriodicTableExs(BST<ElementAtomicNumber> treeAtomicNumber, BST<ElementAtomicMass> treeAtomicMass, BST<ElementByName> treeByName, BST<ElementBySymbol> treeBySymbol, BST<ElementDiscoveredYearDisc> treeByDiscoveredYearDisc) {
        this.treeAtomicNumber = treeAtomicNumber;
        this.treeAtomicMass = treeAtomicMass;
        this.treeByName = treeByName;
        this.treeBySymbol = treeBySymbol;
        this.treeByDiscoveredYearDisc = treeByDiscoveredYearDisc;
    }

    /**
     * Constructor of PeriodicTableExs
     */
    public PeriodicTableExs() {

    }
//----------------CREATE TREES-----------------------

    /**
     * create de Trees
     *
     * @param file_name
     * @throws FileNotFoundException
     */
    public void treeCreate(String file_name) throws FileNotFoundException {
        treeAtomicMass = new BST<>();
        treeAtomicNumber = new BST<>();
        treeByName = new BST<>();
        treeBySymbol = new BST<>();
        treeByDiscoveredYearDisc = new BST<>();
        LinkedList<Element> lst = ReadFromFile.readElements(file_name);
        for (Element ele : lst) {
            treeAtomicMass.insert(new ElementAtomicMass(ele));
            treeAtomicNumber.insert(new ElementAtomicNumber(ele));
            treeByName.insert(new ElementByName(ele));
            treeBySymbol.insert(new ElementBySymbol(ele));
            treeByDiscoveredYearDisc.insert(new ElementDiscoveredYearDisc(ele));
        }
    }

    /**
     * @return the bst of the element by discovered and year of discorver
     */
    public BST<ElementDiscoveredYearDisc> getTreeDiscoveredYear() {
        return this.treeByDiscoveredYearDisc;
    }

//----------------EX 1A-----------------------
    /**
     * Find the element by the atomic number
     * @param atomic_number
     * @throws FileNotFoundException
     * @return element
     */
    public ElementAtomicNumber findElementByAtomicNumber(Integer atomic_number) throws FileNotFoundException {
        if (treeAtomicNumber == null) {
            return null;
        }
        ElementAtomicNumber ean = new ElementAtomicNumber(atomic_number);
        return treeAtomicNumber.find(ean);
    }

    /**
     * Find the element by the atomic mass
     * @param atomic_mass
     * @throws FileNotFoundException
     * @return element
     */
    public ElementAtomicMass findElementByAtomicMass(Double atomic_mass) throws FileNotFoundException {
        if (treeAtomicMass == null) {
            return null;
        }
        ElementAtomicMass eam = new ElementAtomicMass(atomic_mass);
        return treeAtomicMass.find(eam);
    }

    /**
     * Find the element by the element name
     * @param name
     * @throws FileNotFoundException
     * @return element
     */
    public ElementByName findElementByName(String name) throws FileNotFoundException {
        if (treeByName == null) {
            return null;
        }
        ElementByName ebn = new ElementByName(name);
        return treeByName.find(ebn);
    }

    /**
     * Find the element by the element symbol
     * @param symbol
     * @throws FileNotFoundException
     * @return element
     */
    public ElementBySymbol findElementBySymbol(String symbol) throws FileNotFoundException {
        if (treeBySymbol == null) {
            return null;
        }
        ElementBySymbol ebs = new ElementBySymbol(symbol);
        return treeBySymbol.find(ebs);
    }
//----------------EX 1B-----------------------

    /**
     * Find the elements within an interval
     * @param min
     * @param max
     * @return bst
     */
    public BST<ElementDiscoveredYearDisc> searchInMinMax(Double min, Double max) {
        BST<ElementDiscoveredYearDisc> onInterval = new BST<>();
        for (ElementDiscoveredYearDisc atomicMass : treeByDiscoveredYearDisc.inOrder()) {
            if (atomicMass.getAtomic_mass() < max && atomicMass.getAtomic_mass() > min) {
                onInterval.insert(atomicMass);
            }
        }
        return onInterval;
    }

    /**
     * Creates a map with the type, phase and the ocurrences depending on this
     * @param onInterval
     * @return bst
     */
    public Map<String, PhaseByType> occurencesByTypeAndPhase(BST<ElementDiscoveredYearDisc> onInterval) {
        Map<String, PhaseByType> mapWithInfo = new LinkedHashMap<>();
        PhaseByType calcOccurences;
        for (ElementDiscoveredYearDisc element : onInterval.inOrder()) {
            if (!mapWithInfo.containsKey(element.getType())) {
                mapWithInfo.put(element.getType(), new PhaseByType(element.getType()));
            }
            calcOccurences = mapWithInfo.get(element.getType());
            switch (element.getPhase()) {
                case "artificial":
                    calcOccurences.incArtificialCount();
                    break;
                case "gas":
                    calcOccurences.incGasCount();
                    break;
                case "liq":
                    calcOccurences.incLiquidCount();
                    break;
                case "solid":
                    calcOccurences.incSolidCount();
                    break;
            }
        }
        return mapWithInfo;
    }
//------------------------EX 2A-------------------------------

    /**
     * Creates and fills a map with the number of the ocurrences and the eletron configuration
     * @param treeByEletronConf
     * @return bst
     */
    public Map<Integer, List<String>> getOcurrences(BST<ElementByEletronicConf> treeByEletronConf) {
        Map<Integer, List<String>> mapOfOcurrences = new LinkedHashMap<>();
        for (ElementByEletronicConf e : treeByEletronConf.inOrder()) {
            if (!mapOfOcurrences.containsKey(e.getOcurrences())) {
                mapOfOcurrences.put(e.getOcurrences(), new ArrayList<>());
            }
            List<String> lst = mapOfOcurrences.get(e.getOcurrences());
            lst.add(e.getElectron_configuration());
        }
        return mapOfOcurrences;
    }

    /**
     * Calculate the number of ocurrences
     * @param tree
     * @param lhs
     * @param list
     */
    public void calcOcurr(BST<ElementByEletronicConf> tree, LinkedHashSet<String> lhs, ArrayList<String> list) {
        for (String s : lhs) {
            int ocurr = 0;
            for (String aux : list) {
                if (s.equals(aux)) {
                    ocurr++;
                }
            }
            if (ocurr > 1) {
                tree.insert(new ElementByEletronicConf(s, ocurr));
            }
        }
    }

    /**
     * Check the eletron configuration and sends the information to create the map
     * @param listWithData
     * @return map
     */
    public Map<Integer, List<String>> ocurrencesByEletronConf(LinkedList<Element> listWithData) {
        if (listWithData == null) {
            return null;
        }
        BST<ElementByEletronicConf> treeByEletronConf = new AVL<>();
        LinkedHashSet<String> lhs = new LinkedHashSet<>();
        ArrayList<String> list = new ArrayList<>();
        for (Element ele : listWithData) {
            if (!"".equals(ele.getElectron_configuration())) {
                String[] split = ele.getElectron_configuration().split(" ");
                String configuration = "";
                for (int i = 0; i < split.length; i++) {
                    configuration += split[i];
                    lhs.add(configuration);
                    list.add(configuration);
                    configuration += " ";
                }
            }
        }
        calcOcurr(treeByEletronConf, lhs, list);
        Map<Integer, List<String>> finalMap = getOcurrences(treeByEletronConf);
        return finalMap;
    }

//----------------------------------EX 2B-------------------------------------
    
    /**
     * Creates a tree with the information of the method of exercice 1 b)
     * @param listWithData
     * @return BST
     */
    public BST<ElementByEletronicConf> treeByEletronConf(LinkedList<Element> listWithData) {
        if (listWithData == null) {
            return null;
        }
        BST<ElementByEletronicConf> treeByEletronConf = new AVL<>();
        for (Integer ocurr : ocurrencesByEletronConf(listWithData).keySet()) {
            if (ocurr > 2) {
                for (String config : ocurrencesByEletronConf(listWithData).get(ocurr)) {
                    treeByEletronConf.insert(new ElementByEletronicConf(config, ocurr));
                }
            }
        }
        return treeByEletronConf;
    }
//----------------------------------EX 2C-------------------------------------

    /**
     * Gets the most distant elements and return that information and the distance
     * @param treeByEletronConf
     * @return pair
     */
    public Pair<Pair<ElementByEletronicConf, ElementByEletronicConf>, Integer> getMostDistanceElectronConf(BST<ElementByEletronicConf> treeByEletronConf) {
        if (treeByEletronConf == null) {
            return null;
        }
        Pair<Pair<ElementByEletronicConf, ElementByEletronicConf>, Integer> par = null;
        int diameter = treeByEletronConf.diameter();
        for (ElementByEletronicConf element : treeByEletronConf.inOrder()) {
            List<ElementByEletronicConf> lst = treeByEletronConf.kdistanceNode(element, diameter);
            if (!lst.isEmpty()) {
                for (int i = 0; i < lst.size(); i++) {
                    Pair<ElementByEletronicConf, ElementByEletronicConf> aux = new Pair<>(element, lst.get(i));
                    par = new Pair<>(aux, diameter);
                }
            }
        }
        return par;
    }
//----------------------------------EX 2D-------------------------------------

    /**
     * Transforms a bst in a complet tree
     * @param treeByEletronConf
     * @return tree
     */
    public TREE<ElementByEletronicConf> completeTree(BST<ElementByEletronicConf> treeByEletronConf) {
        ArrayList<ElementByEletronicConf> arr = new ArrayList<>();
        for (ElementByEletronicConf elementByEletronicConf : treeByEletronConf.inOrder()) {
            arr.add(elementByEletronicConf);
        }
        TREE<ElementByEletronicConf> treeCompleteEletronConfig = new TREE<>();
        treeCompleteEletronConfig.constructCompleteTree(arr);
        return treeCompleteEletronConfig;
    }
}
