/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author andre
 */
public class PhaseByType{
    private String type;
    
    private int artificial;
    
    private int solid;
    
    private int gas;
    
    private int liquid;

    public PhaseByType(String type) {
        this.type = type;
        this.artificial = 0;
        this.solid = 0;
        this.gas = 0;
        this.liquid = 0;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getArtificial() {
        return artificial;
    }

    public void setArtificial(int artificial) {
        this.artificial = artificial;
    }

    public int getSolid() {
        return solid;
    }

    public void setSolid(int solid) {
        this.solid = solid;
    }

    public int getGas() {
        return gas;
    }

    public void setGas(int gas) {
        this.gas = gas;
    }

    public int getLiquid() {
        return liquid;
    }
    
    public int getTotal(){
        return this.artificial+this.gas+this.liquid+this.solid;
    }

    public void setLiquid(int liquid) {
        this.liquid = liquid;
    }
    
    public void incArtificialCount(){
        this.artificial++;
    }
    
    public void incGasCount(){
        this.gas++;
    }
    
    public void incSolidCount(){
        this.solid++;
    }
    
    public void incLiquidCount(){
        this.liquid++;
    }
    
}
