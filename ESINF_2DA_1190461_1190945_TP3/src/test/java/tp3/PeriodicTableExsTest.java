/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import Tree.BST;
import Tree.TREE;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javafx.util.Pair;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andre
 */
public class PeriodicTableExsTest {

    PeriodicTableExs instance;

    public PeriodicTableExsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws FileNotFoundException {
        instance = new PeriodicTableExs();
        instance.treeCreate("Periodic Table of Elements.csv");
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of treeCreate method, of class PeriodicTableExs.
     */
    @Test
    public void testTreeCreate() throws Exception {
        System.out.println("treeCreate");
        PeriodicTableExs instance = new PeriodicTableExs();
        instance.treeCreate("Periodic Table of Elements.csv");
        assertTrue(!instance.getTreeDiscoveredYear().isEmpty());
    }

    /**
     * Test of findElementByAtomicNumber method, of class PeriodicTableExs.
     */
    @Test
    public void testFindElementByAtomicNumber() throws Exception {
        setUp();
        System.out.println("findElementByAtomicNumber");
        Integer atomic_number = 20;
        ElementAtomicNumber result = instance.findElementByAtomicNumber(atomic_number);
        assertEquals("Ca", result.getSymbol());

        Integer atomic_number2 = 18;
        ElementAtomicNumber result2 = instance.findElementByAtomicNumber(atomic_number2);
        assertEquals("Ar", result2.getSymbol());

    }

    /**
     * Test of findElementByAtomicMass method, of class PeriodicTableExs.
     */
    @Test
    public void testFindElementByAtomicMass() throws Exception {
        System.out.println("findElementByAtomicMass");
        setUp();
        Double atomic_mass = 7.0;
        ElementAtomicMass result = instance.findElementByAtomicMass(atomic_mass);
        assertEquals("Li", result.getSymbol());

        Double atomic_mass2 = 28.085;
        ElementAtomicMass result2 = instance.findElementByAtomicMass(atomic_mass2);
        assertEquals("Si", result2.getSymbol());
    }

    /**
     * Test of findElementByName method, of class PeriodicTableExs.
     */
    @Test
    public void testFindElementByName() throws Exception {
        System.out.println("findElementByName");
        setUp();
        String name = "Oxygen";
        ElementByName result = instance.findElementByName(name);
        assertEquals("O", result.getSymbol());

        String name2 = "Vanadium";
        ElementByName result2 = instance.findElementByName(name2);
        assertEquals("V", result2.getSymbol());
    }

    /**
     * Test of findElementBySymbol method, of class PeriodicTableExs.
     */
    @Test
    public void testFindElementBySymbol() throws Exception {
        System.out.println("findElementBySymbol");
        setUp();
        String symbol = "O";
        ElementBySymbol result = instance.findElementBySymbol(symbol);
        assertEquals("Oxygen", result.getElement());

        String symbol2 = "V";
        ElementBySymbol result2 = instance.findElementBySymbol(symbol2);
        assertEquals("Vanadium", result2.getElement());
    }

    /**
     * Test of searchInMinMax method, of class PeriodicTableExs.
     */
    @Test
    public void testSearchInMinMax() throws FileNotFoundException {
        System.out.println("searchInMinMax");
        setUp();
        Double min = 20.0;
        Double max = 65.0;
        BST<ElementDiscoveredYearDisc> result = instance.searchInMinMax(min, max);

        PeriodicTableExs pt = new PeriodicTableExs();
        LinkedList<ElementDiscoveredYearDisc> lst1 = new LinkedList<>();
        pt.treeCreate("TestsForEx1a.txt");
        for (ElementDiscoveredYearDisc element : pt.getTreeDiscoveredYear().inOrder()) {
            lst1.add(element);
        }
        LinkedList<ElementDiscoveredYearDisc> lst2 = new LinkedList<>();
        for (ElementDiscoveredYearDisc element : result.inOrder()) {
            lst2.add(element);
        }

        assertEquals(lst1.size(), lst2.size());

        for (int i = 0; i < lst1.size(); i++) {
            assertEquals(lst1.get(i).getAtomic_number(), lst2.get(i).getAtomic_number());
        }
    }

    /**
     * Test of occurencesByTypeAndPhase method, of class PeriodicTableExs.
     */
    @Test
    public void testOccurencesByTypeAndPhase() {
        System.out.println("occurencesByTypeAndPhase");
        BST<ElementDiscoveredYearDisc> onInterval = instance.searchInMinMax(20.0, 65.0);
        Map<String, PhaseByType> expResult = new LinkedHashMap<>();

        PhaseByType p8 = new PhaseByType("Transition Metal");
        p8.setSolid(9);
        PhaseByType p1 = new PhaseByType("Alkali Metal");
        p1.setSolid(2);
        PhaseByType p2 = new PhaseByType("Alkaline Earth Metal");
        p2.setSolid(2);
        PhaseByType p3 = new PhaseByType("Halogen");
        p3.setGas(1);
        PhaseByType p4 = new PhaseByType("Metal");
        p4.setSolid(1);
        PhaseByType p5 = new PhaseByType("Metalloid");
        p5.setSolid(1);
        PhaseByType p6 = new PhaseByType("Noble Gas");
        p6.setGas(2);
        PhaseByType p7 = new PhaseByType("Nonmetal");
        p7.setSolid(2);

        expResult.put("Transition Metal", p8);
        expResult.put("Metalloid", p5);
        expResult.put("Alkaline Earth Metal", p2);
        expResult.put("Nonmetal", p7);
        expResult.put("Alkali Metal", p1);
        expResult.put("Noble Gas", p6);
        expResult.put("Halogen", p3);
        expResult.put("Metal", p4);

        Map<String, PhaseByType> result = instance.occurencesByTypeAndPhase(onInterval);

        assertEquals(expResult.size(), result.size());

        for (String type : result.keySet()) {
            assertTrue(expResult.containsKey(type));
            assertEquals(expResult.get(type).getTotal(), result.get(type).getTotal());
        }
    }

    /**
     * Test of ocurrencesByEletronConf method, of class PeriodicTableExs.
     */
    @Test
    public void testOcurrencesByEletronConf() throws FileNotFoundException {
        System.out.println("ocurrencesByEletronConf");
        LinkedList<Element> listWithData = ReadFromFile.readElements("Periodic Table of Elements.csv");
        List<String> aux = new ArrayList<>();
        aux.add("[Xe]");
        Map<Integer, List<String>> expResult = new TreeMap<>();
        expResult.put(32, aux);
        
        List<String> aux2 = new ArrayList<>();
        aux2.add("[Ar]");
        aux2.add("[Kr]");
        expResult.put(18, aux2);
        
        List<String> aux3 = new ArrayList<>();
        aux3.add("[Xe] 4f14");
        expResult.put(17, aux3);
        
        List<String> aux4 = new ArrayList<>();
        aux4.add("[Kr] 4d10");
        aux4.add("[Rn]");
        expResult.put(9, aux4);
        
        List<String> aux5 = new ArrayList<>();
        aux5.add("[Ar] 3d10");
        aux5.add("[He]");
        aux5.add("[Ne]");
        aux5.add("[Xe] 4f14 5d10");
        expResult.put(8, aux5);
        
        List<String> aux6 = new ArrayList<>();
        aux6.add("[Ar] 3d10 4s2");
        aux6.add("[He] 2s2");
        aux6.add("[Kr] 4d10 5s2");
        aux6.add("[Ne] 3s2");
        aux6.add("[Xe] 4f14 5d10 6s2");
        expResult.put(7, aux6);
        
        List<String> aux7 = new ArrayList<>();
        aux7.add("[Ar] 3d5");
        aux7.add("[Kr] 4d5");
        aux7.add("[Xe] 4f7");
        expResult.put(2, aux7);
        
        Map<Integer, List<String>> result = instance.ocurrencesByEletronConf(listWithData);
        assertEquals(expResult, result);
    }

    /**
     * Test of treeByEletronConf method, of class PeriodicTableExs.
     */
    @Test
    public void testTreeByEletronConf() throws FileNotFoundException {
        System.out.println("treeByEletronConf");
        LinkedList<Element> listWithData = ReadFromFile.readElements("Periodic Table of Elements.csv");
        
        LinkedList<ElementByEletronicConf> lst1 = new LinkedList<>();

        BST<ElementByEletronicConf> result = instance.treeByEletronConf(listWithData);
        LinkedList<ElementByEletronicConf> lst2 = new LinkedList<>();
        
        for (ElementByEletronicConf element : result.inOrder()) {
            lst2.add(element);
        }
        
        lst1.add(new ElementByEletronicConf("[Xe]", 32));
        lst1.add(new ElementByEletronicConf("[Ar]", 18));
        lst1.add(new ElementByEletronicConf("[Kr]", 18));
        lst1.add(new ElementByEletronicConf("[Xe] 4f14", 17));
        lst1.add(new ElementByEletronicConf("[Kr] 4d10", 9));
        lst1.add(new ElementByEletronicConf("[Rn]", 9));
        lst1.add(new ElementByEletronicConf("[Ar] 3d10", 8));
        lst1.add(new ElementByEletronicConf("[He]", 8));
        lst1.add(new ElementByEletronicConf("[Ne]", 8));
        lst1.add(new ElementByEletronicConf("[Xe] 4f14 5d10", 8));
        lst1.add(new ElementByEletronicConf("[Ar] 3d10 4s2", 7));
        lst1.add(new ElementByEletronicConf("[He] 2s2", 7));
        lst1.add(new ElementByEletronicConf("[Kr] 4d10 5s2", 7));
        lst1.add(new ElementByEletronicConf("[Ne] 3s2", 7));
        lst1.add(new ElementByEletronicConf("[Xe] 4f14 5d10 6s2", 7));
        
        assertEquals(lst1.size(), lst2.size());
        
        for (int i = 0; i < lst1.size(); i++) {
            assertEquals(lst1.get(i).getOcurrences(), lst2.get(i).getOcurrences());
            assertEquals(lst1.get(i).getAtomic_number(), lst2.get(i).getAtomic_number());
        }
    }

    /**
     * Test of getMostDistanceElectronConf method, of class PeriodicTableExs.
     */
    @Test
    public void testGetMostDistanceElectronConf() throws FileNotFoundException {
        System.out.println("getMostDistanceElectronConf");
        BST<ElementByEletronicConf> treeByEletronConf = instance.treeByEletronConf(ReadFromFile.readElements("Periodic Table of Elements.csv"));
        Pair<Pair<ElementByEletronicConf, ElementByEletronicConf>, Integer> expResult;
        Pair<ElementByEletronicConf, ElementByEletronicConf> aux;

        aux = new Pair<>(new ElementByEletronicConf("[Xe] 4f14 5d10 6s2", 7),new ElementByEletronicConf("[Ar] 3d10", 8));
        
        expResult = new Pair<>(aux, 6);
        Pair<Pair<ElementByEletronicConf, ElementByEletronicConf>, Integer> result = instance.getMostDistanceElectronConf(treeByEletronConf);
        assertEquals(expResult.getKey().getKey().getAtomic_number(), result.getKey().getKey().getAtomic_number());
        assertEquals(expResult.getKey().getValue().getAtomic_number(), result.getKey().getValue().getAtomic_number());
        assertEquals(expResult.getValue(), result.getValue());
    }

    /**
     * Test of exd method, of class PeriodicTableExs.
     */
    @Test
    public void testCompleteTree() throws FileNotFoundException {
        System.out.println("completeTree");
        BST<ElementByEletronicConf> tree = instance.treeByEletronConf(ReadFromFile.readElements("Periodic Table of Elements.csv"));
        
        LinkedList<ElementByEletronicConf> lst1 = new LinkedList<>();
        
        TREE<ElementByEletronicConf> result = instance.completeTree(tree);
        
        for (ElementByEletronicConf element : result.inOrder()) {
            lst1.add(element);
        }
        
        LinkedList<ElementByEletronicConf> lst2 = new LinkedList<>();
        
        lst2.add(new ElementByEletronicConf("[He]", 8));
        lst2.add(new ElementByEletronicConf("[Xe] 4f14", 17));
        lst2.add(new ElementByEletronicConf("[Ne]", 8));
        lst2.add(new ElementByEletronicConf("[Ar]", 18));
        lst2.add(new ElementByEletronicConf("[Xe] 4f14 5d10", 8));
        lst2.add(new ElementByEletronicConf("[Kr] 4d10", 9));
        lst2.add(new ElementByEletronicConf("[Ar] 3d10 4s2", 7));
        lst2.add(new ElementByEletronicConf("[Xe]", 32));
        lst2.add(new ElementByEletronicConf("[He] 2s2]", 7));
        lst2.add(new ElementByEletronicConf("[Rn]", 9));
        lst2.add(new ElementByEletronicConf("[Kr] 4d10 5s2", 7));
        lst2.add(new ElementByEletronicConf("[Kr]", 18));
        lst2.add(new ElementByEletronicConf("[Ne] 3s2", 7));
        lst2.add(new ElementByEletronicConf("[Ar] 3d10", 8));
        lst2.add(new ElementByEletronicConf("[Xe] 4f14 5d10 6s2", 7));
        
        assertEquals(lst2.size(), lst1.size());
        
        for (int i = 0; i < lst1.size(); i++) {
            assertEquals(lst2.get(i).getAtomic_number(), lst1.get(i).getAtomic_number());
        }
    }

}
