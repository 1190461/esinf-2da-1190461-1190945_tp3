/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andre
 */
public class ReadFromFileTest {
    
    public ReadFromFileTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readElements method, of class ReadFromFile.
     */
    @Test
    public void testReadElements() throws Exception {
        System.out.println("readElements");
        String file_name = "Periodic Table of Elements.csv";
        Integer expResult = 1;
        LinkedList<Element> lst = ReadFromFile.readElements(file_name);
        Integer result = lst.get(0).getAtomic_number();
        assertEquals(expResult, result);
        
        Integer expResult2 = 10;
        Integer result2 = lst.get(9).getAtomic_number();
        assertEquals(expResult2, result2);
        
        Integer expResult3 = 100;
        Integer result3 = lst.get(99).getAtomic_number();
        assertEquals(expResult3, result3);
    }

    /**
     * Test of lerLinha method, of class ReadFromFile.
     */
    @Test
    public void testLerLinha() {
        System.out.println("lerLinha");
        String line = "5,Boron,B,10.811,10.81,2,13,solid,rho,Metalloid,0.23,1.2,2.04,8.298,2.34,2573.15,4200,6,Gay-Lussac,1808,1.026,[He] 2s2 2p1,2,13";
        Integer expResult = 5;
        Integer result = ReadFromFile.lerLinha(line).getAtomic_number();
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyDouble method, of class ReadFromFile.
     */
    @Test
    public void testVerifyDouble() {
        System.out.println("verifyDouble");
        String test = "2";
        Double expResult = 2.0;
        Double result = ReadFromFile.verifyDouble(test);
        assertEquals(expResult, result);
    }

    /**
     * Test of verifyInt method, of class ReadFromFile.
     */
    @Test
    public void testVerifyInt() {
        System.out.println("verifyInt");
        String test = "";
        Integer expResult = 0;
        Integer result = ReadFromFile.verifyInt(test);
        assertEquals(expResult, result);
    }
    
}
